#pragma once

#include <TROOT.h>

class psPixel {
  //  double x
 public:
  double x;
  double y;
  double e;

  ClassDef(psPixel,1)
    };

class psCluster {
  //  double x
 public:
  double x;
  double y;
  double e;

  ClassDef(psCluster,1)
    };
