#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class psPixel+;
#pragma link C++ class psCluster+;
#pragma link C++ class vector<psPixel>+;
#pragma link C++ class vector<psCluster>+;
#endif
