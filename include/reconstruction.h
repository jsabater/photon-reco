#pragma once
#include <vector>
#include <TROOT.h>
#include <iostream>
/*
class Simple {       // The class
 public:             // Access specifier
  int myNum;        // Attribute (int variable)
  std::string myString;  // Attribute (string variable)

  //  ClassDef(Simple,1)
};
*/

/*
class Simple {
  double x;

 public:
 Simple() : x(2.5) {}
  double GetX() const;

  ClassDef(Simple,1)
    };

// member function definition
double Simple::GetX() const {return x;}
*/
  
  

// function to flatten a 2D vector
std::vector<double> flatten_2dVector(std::vector<std::vector<double>>& v) {
  std::vector<double> v_flat;
  for (int i = 0; i < v.size(); i++){
    for (int j = 0; j < v[i].size(); j++) {
      v_flat.push_back(v[i][j]);
    }
  }  

  return v_flat;

}
// function to find the maximum and position of the maximum of a 2d vector
std::vector<double> find_max_and_position(std::vector<std::vector<double>>& v) {
  double max = 0;
  double pixelX = 0;
  double pixelY = 0;
  for (int i = 0; i < v.size(); i++){
    for (int j = 0; j < v[i].size(); j++) {
      if (v[i][j] > max) {
	max = v[i][j];
	pixelX = static_cast<int>(i);
	pixelY = static_cast<int>(j);
      }
    }
  }
  std::vector<double> v_max_and_position = {max,pixelX,pixelY};
  
  return v_max_and_position;
}
// function to find the 2nd maximum and position of the maximum of a 2d vector
// it also requires the second maximum to be away from the 1st by at least 1 pixel
// todo: it should (dx > 1) || (dy > 3) instead of (dx > 1) && (dy > 1)
std::vector<double> find_2nd_max_and_position(std::vector<std::vector<double>>& v, double max_1st, int max_x, int max_y) {
  double max = 0;
  double pixelX = 0;
  double pixelY = 0;
  double dx = 0;
  double dy = 0;
  for (int i = 0; i < v.size(); i++){
    for (int j = 0; j < v[i].size(); j++) {
      if (v[i][j] > max && v[i][j] != max_1st) {
	// separation between 1st and 2nd max by at least 1 pixel
	dx = abs(max_x - i);
	dy = abs(max_y - j);
	if ((dx > 1) || (dy > 3)) {
	  max = v[i][j];
	  pixelX = static_cast<int>(i);
	  pixelY = static_cast<int>(j);
	}
      }
    }
  }

  std::vector<double> v_max_and_position = {max,pixelX,pixelY};
  
  return v_max_and_position;
}
// Function to calculate the ifference between the second energy maximum and the minimal energy deposit in the valley between the maximal energy deposit and the second energy maximum
double calc_edmax(std::vector<std::vector<double>>& v, double max_1st, int max_x, int max_y, double max_2nd, int max_2nd_x, int max_2nd_y) {
  double e3 = 0;
  double e1 = 0;
  //  std::vector<pair<double,double>> ;
  std::vector<double> e_pixelMin;
  // distance between 1st and 2nd maximum
  double dx_1st_2nd = abs(max_x - max_2nd_x);
  double dy_1st_2nd = abs(max_y - max_2nd_y);
  for (int i = 0; i < v.size(); i++){
    // distance in x from maximum (in pixel units)
    int dx = abs(max_x - i);
    for (int j = 0; j < v[i].size(); j++) {
      // distance in y from maximum (in pixels units)
      int dy = abs(max_y - j);
      // Choose only pixels that are between the maximum and 2nd maximum energy deposits
      if ((dx <= dx_1st_2nd) && (dy <= dy_1st_2nd)) {
        e_pixelMin.push_back(v[i][j]);
      }

    }
  }

  // sort energies in ascending order
  std::sort(e_pixelMin.begin(), e_pixelMin.end());    
  // grab the lowest value different than zero
  double ePixelMin = 0;
  for (int i = 0; i < e_pixelMin.size(); i++) {
    if (e_pixelMin[i] > 0) {
      ePixelMin = e_pixelMin[i];
      break;
    }
  }
  // calcualte the difference 
  double edmax = max_2nd - ePixelMin;
  return edmax;
}

// Function to find the energy deposited outside the shower core.
double calc_eocore(std::vector<std::vector<double>>& v, double max_1st, int max_x, int max_y, int n) {
  double e3 = 0;
  double e1 = 0;
  for (int i = 0; i < v.size(); i++){
    int dx = abs(max_x - i);
    for (int j = 0; j < v[i].size(); j++) {
      int dy = abs(max_y - j);
      // Choose only pixels that are within +/- n pixels around (and including) the pixel with maximum energy deposit
      // and sum the energies deposited in these pixels.
      if ((dx <= n) && (dy <= n)) {
	e3 += v[i][j];
      }
      // Choose only pixels that are within +/- 1 pixels around (and including) the pixel with maximum energy deposit
      // and sum the energies deposited in these pixels.
      if ((dx <= 1) && (dy <= 1)) {
	e1 += v[i][j];
      }
    }
  }

  double eocore = (e3 - e1) / e1;
  return eocore;
}
// Function for calculating the shower width on n pixels in pixel units. imax describes the pixel with maximum energy deposit.
double calc_wnst(std::vector<std::vector<double>>& v, double max_1st, int max_x, int max_y, int n) {
  double nominator = 0;
  double denominator = 0;
  for (int i = 0; i < v.size(); i++){
    int dx = abs(max_x - i);
    for (int j = 0; j < v[i].size(); j++) {
      int dy = abs(max_y - j);
      // Choose only pixels that are within +/- n pixels around (and including) the pixel with maximum energy deposit
      // and sum the energies deposited in these pixels.
      if ((dx <= n) && (dy <= n)) {
        nominator += v[i][j] * (i + j - max_x - max_y) * (i + j - max_x - max_y);
	denominator += v[i][j];
      }
    }
  }

  double wnst = 0;
  if (denominator > 0)
    wnst = sqrt(nominator/denominator);
  return wnst;
}

// function to get the pixel number in X
// get the ID of the tower
// the convention is: left-most tower ID = 0
//                   second from left ID = 1
//                   ...
//                   right-most tower ID = segmentation
int towerIdX(double detectorX, double m_deltaXTower, double aX) {
  //int id = floor((aX + detectorX) / m_deltaXTower); // for detector origin at the centre of the detector
  //  std::cout << "m_deltaXTower " << m_deltaXTower << std::endl;
  //  std::cout << "ax " << aX << std::endl;
  int id = floor(aX / m_deltaXTower); // for detector origin at x=0 and y=0
  return id;
}
// function to get the pixel number in Y
// get the ID of the tower
// the convention is: bottom-most tower ID = 0
//                   second from bottom ID = 1
//                   ...
//                   top-most tower ID = segmentation
int towerIdY(double detectorY, double m_deltaYTower, double aY) {
  //int id = floor((aY + detectorY) / m_deltaYTower);  // for detector origin at the centre of the detector
  int id = floor(aY / m_deltaYTower); // for detector origin at x=0 and y=0
  //  if (id < 0) std::cout << "id negative!" << std::endl;
  return id;
}
int test_towerIdY(double detectorY, int segmentation, double y) {
  int id = floor( (y + detectorY) / (detectorY*2/segmentation) );
  return id;
}
/**  Correct way to access the neighbour of the Y tower, taking into account the full coverage in Y.
 *   Full coverage means that first tower in Y, with ID = 0 is a direct neighbour
 *   of the last tower in y with ID = m_nYTower - 1).
 *   @param[in] aIY requested ID of a y tower, may be < 0 or >= m_nYTower
 *   @return  ID of a tower - shifted and corrected (in [0, m_nYTower) range)
 */
int yNeighbour(int aIY, int m_nYTower) {
  /*
  // in case our detector was cylindrical, we would have to make sure the last and first ID's are next to each other
  // in our case the detector is a plane, the aIY = 0 pixels has no neighbour on the left and the aIY = m_nYTower has no neighbour on the right
  if (aIY < 0) {
    return m_nYTower + aIY;
  } else if (aIY >= m_nYTower) {
    //std::cout << "towwerIDy = " << aIY << " m_nYTower " << m_nYTower << std::endl;
    //	std::cout << "result" << aIY % m_nYTower << std::endl;
    return aIY % m_nYTower;
  }
  */
  return aIY;
}
// function to get the position in X given the pixel number in X
double towerX(int aIdX, double m_deltaXTower, double m_xMax) {
  // middle of the tower
  //return ((aIdX + 0.5) * m_deltaXTower - m_xMax); // for detector origin at the centre of the detector
  return ((aIdX + 0.5) * m_deltaXTower); // for detector origin at x=0 and y=0
}
// function to get the position in Y given the pixel number in Y
double towerY(int aIdY, double m_deltaYTower, double m_yMax) {
  // middle of the tower
  //return ((aIdY + 0.5) * m_deltaYTower - m_yMax); // for detector origin at the centre of the detector
  return ((aIdY + 0.5) * m_deltaYTower); // for detector origin at x=0 and y=0
}
