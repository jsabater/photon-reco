// Sliding window reconstruction algorithm

#include "TSystem.h"
#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH2.h"
#include "TH2F.h"
#include "TROOT.h"
#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <algorithm>
#include "TSystemDirectory.h"
#include "TList.h"
#include <sys/stat.h>
#include <chrono>
#include "TMath.h"

#include "TCanvas.h"

#include "reconstruction.h"
#include "dict.h"


void reconstruction(TString fileSuffix="5", TString senergy = "random", TString geometry = "3p5W", TString nPhotons = "2", TString separation = "50", 
		    TString senergyThreshold = "1", int m_nXWindow = 2, int m_nYWindow = 3, bool straight = false) {

  bool doNeutrinos = false;
  if (nPhotons == "11") {
    std::cout << "shooting neutrinos" << std::endl;
    doNeutrinos = true;
  }
  else {
    if (straight) 
      std::cout << "shooting straight photons" << std::endl;
    else
      std::cout << "shooting angle photons" << std::endl;
  }

  // Count computing time
  clock_t t;
  t = clock();

  const bool debug = false; // to debug
  const bool doTest = false; //  for testing
  const bool doFinalTowers = false;  // to create a final tower with different size of the window size
  const bool doChargeThreshold = true; // to impose lower and upper pixel charge threshold. This only is needed if there are more than one hit in the same pixel, in simulations you usually only have one hit per pixel, therefore it is only required to set a threshold on the 'hit'
  const bool doPixelChargeThreshold = false;
  const double qToGeV =  (6250. * 3.6) / (1.e9); // multiply this number times the charge to obtain the energy deposited
  const bool doLayersMerged = true;  // to construct towers from all layers
  const bool doThirdLayer = false; // only do towers in one layer
  const bool m_ellipseFinalCluster = false; // to require an elliptic shape when building the Final Window

  // window size
  // 2x3
  //int m_nXWindow = 2;
  //int m_nYWindow = 3;
  // 3x5
  //int m_nXWindow = 3;
  //int m_nYWindow = 5;
  TString s_nXWindow = "";
  TString s_nYWindow = "";
  s_nXWindow += m_nXWindow;
  s_nYWindow += m_nYWindow;
  std::cout << "Window size " << m_nXWindow << "x" << m_nYWindow << std::endl;
  
  // define the active region of the detector
  // todo: change the limits from the sensitive region to the total magnet aperture
  // float magnet_aperture = 200; // mm
  float max_x_veto = 132.41;
  float max_y_veto = 169.71;


  TString pathToFile = "/eos/project/f/faser-preshower/simulations/allpix/"+nPhotons+"photon/"+geometry+"/";
  if (doNeutrinos)
    pathToFile = "/eos/project/f/faser-preshower/simulations/allpix/neutrino/";
  
  

  TString input_fileName = pathToFile+"faser_"+fileSuffix+"_"+senergy+"GeV_"+geometry+"_"+nPhotons+"photon_"+separation+"mm.root";
  // different file names if the photons are straight or not when shooting only 1 photon
  if (nPhotons == "1") {
    if (straight)
      input_fileName = pathToFile+"faser_"+fileSuffix+"_"+senergy+"GeV_"+geometry+"_"+nPhotons+"photon_"+separation+"mmstraight.root";
    else
      input_fileName = pathToFile+"faser_"+fileSuffix+"_"+senergy+"GeV_"+geometry+"_"+nPhotons+"photon_"+separation+"mm.root";
  }

  if (doNeutrinos)
    input_fileName = pathToFile+"genie_numu.root";

  // run over several files
  std::vector<TString> file_vec;
  file_vec.push_back(input_fileName);
  
  //  file_vec.push_back("/eos/project/f/faser-preshower/simulations/allpix/1photon/3p5W/faser_4_randomGeV_3p5W_1photon_0mm.root");
  //  file_vec.push_back("/eos/project/f/faser-preshower/simulations/allpix/1photon/3p5W/faser_40_randomGeV_3p5W_1photon_0mm.root");


  /*
  if (doTest) {
    file_vec.clear();
    file_vec.push_back("/afs/cern.ch/user/r/rkotitsa/public/genie/1000_events_genie_tree.root");
  }
  */



  // Detector size a bit expanded
  // Theoretical detector size: x = 129mm, y = 173mm
  const double m_xMax = 140; // used unitl 9 may 2023
  const double m_yMax = 200; // used unitl 9 may 2023
  //  const double m_xMax = 132.41;
  //  const double m_yMax = 169.71;

  
  // divide the detector into towers
  // ideally you want each tower to correspond to a single pixel
  // fixme: segment the dector in X and Y so that each bin coresponds to a single pixel
  // pixels in X = 221*6 = 1326
  // piels in Y = 128*12 = 1536
  
  //  int segmentationX = 100;
  //  int segmentationY = 100;
  // segmentation to have a single bin per pixel
  const int segmentationX = 1436; // used unitl 9 may 2023
  const int segmentationY = 3572; // used unitl 9 may 2023
  //  const int segmentationX = 1326;
  //  const int segmentationY = 1536*2;

  //  double stepX = m_xMax*2./segmentationX;
  //  double stepY = m_yMax*2./segmentationY;



  // hexagon map
  //  TH2Poly *hc = new TH2Poly();
  // a = hexagon side size
  // k = hexagons in X
  // s = hexagons in Y
  //  float a = 0.065; // in mm
  //  int k = 1536;
  //  int s = 1326;
  //  hc->Honeycomb(0,0,a,k,s);
  //  gStyle->SetPalette(53);
  


  // todo: implement this file to energy match in a more automated way
  for (int i = 0; i < file_vec.size(); i++) { 
    double energy = 0;
    if (file_vec[i].Contains("1GeV")) energy = 1;
    if (file_vec[i].Contains("10GeV")) energy = 10;
    if (file_vec[i].Contains("100GeV")) energy = 100;
    if (file_vec[i].Contains("500GeV")) energy = 500;
    if (file_vec[i].Contains("1000GeV")) energy = 1000;
    if (file_vec[i].Contains("3500GeV")) energy = 3500;
    if (file_vec[i].Contains("random")) energy = -1;

    TFile *f = new TFile(file_vec[i],"READONLY");
    f->ls();

    TString sufix = "";
    TTree* myTree = (TTree*)f->Get("myTree");
    if (!myTree) std::cout << "ERROR, could not open tree " << std::endl;

    //    int nev = myTree->GetEntries();
    int nev = 1000; // number of entries of the file. We have to input by hand because a photon may not have interacted and we won't have 1000 entries in the tree but a bit less
    // Since we are running X jobs with N events each, we want to make sure all the events are sequential
    // e.g. 1st file with 100 events, we want the eventID to run from 0 to 99; 2nd file with 100 events, we want the eventID
    // to run from 100 to 199, and so on with the X files we have produced with allpix
    // FIXME: not so hard coded (e.g. parse only the filename and not the entire path)
    //    TString sfileNumber = fileName(73); // this corresponds to the number suffix of the file
    TString sfileNumber = fileSuffix;
    // return the integer contained in the string
    int fileNumber = sfileNumber.Atoi();
    // replace the first char with the number we want
    TString snev = "";
    snev += nev;
    TString snev_modified = snev.Replace(0,1,sfileNumber);

    // input variables to be read from allpix output    
    std::vector<double> *x = 0;
    std::vector<double> *y = 0;
    std::vector<double> *QfC = 0;
    double QfC_l1  = 0;
    double QfC_l2  = 0;
    double QfC_l3  = 0;
    double QfC_l4  = 0;
    double QfC_l5  = 0;
    double QfC_l6  = 0;
    std::vector<int> *layer = 0;
    std::vector<int> *indexX = 0;
    std::vector<int> *indexY = 0;
    ULong64_t eventID = 0;
    int nHits = 0;
    std::vector<int> *terminatingVolume = 0;    

    /*
    std::vector<double> *ePhotonBeam = 0;
    std::vector<double> *xPhotonBeam = 0;
    std::vector<double> *yPhotonBeam = 0;
    std::vector<double> *zPhotonBeam = 0;

    std::vector<double> *xPhotonBeamEnd = 0;
    std::vector<double> *yPhotonBeamEnd = 0;
    std::vector<double> *zPhotonBeamEnd = 0;
    */

    std::vector<double> *eBeam = 0;
    std::vector<double> *xBeam = 0;
    std::vector<double> *yBeam = 0;
    std::vector<double> *zBeam = 0;

    std::vector<double> *xBeamEnd = 0;
    std::vector<double> *yBeamEnd = 0;
    std::vector<double> *zBeamEnd = 0;

    /*
    double ePhotonBeam = 0;
    double xPhotonBeam = 0;
    double yPhotonBeam = 0;
    double zPhotonBeam = 0;
    */

    double QfCtot = 0;
    
    myTree->SetBranchAddress("x",&x);
    myTree->SetBranchAddress("y",&y);
    myTree->SetBranchAddress("indexX",&indexX);
    myTree->SetBranchAddress("indexY",&indexY);
    myTree->SetBranchAddress("QfC",&QfC);
    myTree->SetBranchAddress("QfC_l1",&QfC_l1);
    myTree->SetBranchAddress("QfC_l2",&QfC_l2);
    myTree->SetBranchAddress("QfC_l3",&QfC_l3);
    myTree->SetBranchAddress("QfC_l4",&QfC_l4);
    myTree->SetBranchAddress("QfC_l5",&QfC_l5);
    myTree->SetBranchAddress("QfC_l6",&QfC_l6);
    myTree->SetBranchAddress("QfCtot",&QfCtot);
    myTree->SetBranchAddress("eventID",&eventID);
    myTree->SetBranchAddress("layer",&layer);
    myTree->SetBranchAddress("nHits",&nHits);
    myTree->SetBranchAddress("terminatingVolume",&terminatingVolume);
    /*
    myTree->SetBranchAddress("ePhotonBeam",&ePhotonBeam);
    myTree->SetBranchAddress("xPhotonBeam",&xPhotonBeam);
    myTree->SetBranchAddress("yPhotonBeam",&yPhotonBeam);
    myTree->SetBranchAddress("zPhotonBeam",&zPhotonBeam);
    myTree->SetBranchAddress("xPhotonBeamEnd",&xPhotonBeamEnd);
    myTree->SetBranchAddress("yPhotonBeamEnd",&yPhotonBeamEnd);
    myTree->SetBranchAddress("zPhotonBeamEnd",&zPhotonBeamEnd);
    */

    myTree->SetBranchAddress("eBeam",&eBeam);
    myTree->SetBranchAddress("xBeam",&xBeam);
    myTree->SetBranchAddress("yBeam",&yBeam);
    myTree->SetBranchAddress("zBeam",&zBeam);
    myTree->SetBranchAddress("xBeamEnd",&xBeamEnd);
    myTree->SetBranchAddress("yBeamEnd",&yBeamEnd);
    myTree->SetBranchAddress("zBeamEnd",&zBeamEnd);
    
  

    // output file

    // variables
    std::vector<double> eCluster;
    std::vector<double> eCluster_l1;
    std::vector<double> eCluster_l2;
    std::vector<double> eCluster_l3;
    std::vector<double> eCluster_l4;
    std::vector<double> eCluster_l5;
    std::vector<double> eCluster_l6;
    


    std::vector<double> epreCluster;
    std::vector<double> epreCluster_l1;
    std::vector<double> epreCluster_l2;
    std::vector<double> epreCluster_l3;
    std::vector<double> epreCluster_l4;
    std::vector<double> epreCluster_l5;
    std::vector<double> epreCluster_l6;
    int nCluster = 0;
    int npreCluster = 0;
    std::vector<double> xCluster;
    std::vector<double> xCluster_l1;
    std::vector<double> xCluster_l2;
    std::vector<double> xCluster_l3;
    std::vector<double> xCluster_l4;
    std::vector<double> xCluster_l5;
    std::vector<double> xCluster_l6;
    std::vector<double> xpreCluster;
    std::vector<double> xpreCluster_l1;
    std::vector<double> xpreCluster_l2;
    std::vector<double> xpreCluster_l3;
    std::vector<double> xpreCluster_l4;
    std::vector<double> xpreCluster_l5;
    std::vector<double> xpreCluster_l6;
    std::vector<double> yCluster;
    std::vector<double> yCluster_l1;
    std::vector<double> yCluster_l2;
    std::vector<double> yCluster_l3;
    std::vector<double> yCluster_l4;
    std::vector<double> yCluster_l5;
    std::vector<double> yCluster_l6;
    std::vector<double> ypreCluster;
    std::vector<double> ypreCluster_l1;
    std::vector<double> ypreCluster_l2;
    std::vector<double> ypreCluster_l3;
    std::vector<double> ypreCluster_l4;
    std::vector<double> ypreCluster_l5;
    std::vector<double> ypreCluster_l6;
    double maxCharge_l1;
    double maxCharge_l2;
    double maxCharge_l3;
    double maxCharge_l4;
    double maxCharge_l5;
    double maxCharge_l6;


    double emax;
    double emax_x;
    double emax_y;
    double e2max;
    double e2max_x;
    double e2max_y;
    double d_e12;
    double dx_e12;
    double dy_e12;

    double emax_l2;
    double e2max_l2;
    double eocore_l2;
    double edmax_l2;
    double w3st_l2;
    double eFrac_l2;
    double eFrac1_l2;

    double emax_l6;
    double e2max_l6;
    double eocore_l6;
    double edmax_l6;
    double w3st_l6;
    double w40st_l6;
    double eFrac1_l6;
    
    double eFrac_l3;
    double eFrac_l4;
    double eFrac_l5;
    double eFrac_l6;
    
    std::vector<double> thetaBeam;
    std::vector<double> phiBeam;
    
    //TString senergyThreshold = "10";
    double m_energyThreshold = std::stod(senergyThreshold.Data());
    std::cout << "Energy threshold set to " << m_energyThreshold << " fC" << std::endl;
    //TString outputFileName=senergy+"GeV_"+geometry+"_"+nPhotons+"photon_"+separation+"mm_clusters_"+fileSuffix+"_thr"+senergyThreshold+"fC.root";
    TString outputFileName=senergy+"GeV_"+geometry+"_"+nPhotons+"photon_"+separation+"mm_win"+s_nXWindow+"x"+s_nYWindow+"_clusters_"+fileSuffix+"_thr"+senergyThreshold+"fC.root";
    if (straight)
      //outputFileName=senergy+"GeV_"+geometry+"_"+nPhotons+"photon_"+separation+"mm_clusters_"+fileSuffix+"_straight_thr"+senergyThreshold+"fC.root";
      outputFileName=senergy+"GeV_"+geometry+"_"+nPhotons+"photon_"+separation+"mm_win"+s_nXWindow+"x"+s_nYWindow+"_clusters_"+fileSuffix+"_straight_thr"+senergyThreshold+"fC.root";
    if (doTest) outputFileName = "test.root";
    if (doNeutrinos)  outputFileName = "genie_numu_win"+s_nXWindow+"x"+s_nYWindow+"_thr"+senergyThreshold+"fC.root";
    TFile* outputFile = new TFile("output/"+outputFileName,"RECREATE");
    TString outputTreeName = "clusterTree";
    TTree* clusterTree = new TTree(outputTreeName,outputTreeName); // output tree
    clusterTree->Branch("eventID",&eventID);
    //    clusterTree->Branch("QfC",&QfC);
    //    clusterTree->Branch("x",&x);
    //    clusterTree->Branch("y",&y);
    //    clusterTree->Branch("layer",&layer);

    clusterTree->Branch("eCluster",&eCluster);
    clusterTree->Branch("nCluster",&nCluster);
    clusterTree->Branch("npreCluster",&npreCluster);
    clusterTree->Branch("xCluster",&xCluster);
    clusterTree->Branch("yCluster",&yCluster);  

    /*
    clusterTree->Branch("eCluster_l1",&eCluster_l1);
    clusterTree->Branch("xCluster_l1",&xCluster_l1);
    clusterTree->Branch("yCluster_l1",&yCluster_l1);

    clusterTree->Branch("eCluster_l2",&eCluster_l2);
    clusterTree->Branch("xCluster_l2",&xCluster_l2);
    clusterTree->Branch("yCluster_l2",&yCluster_l2);

    clusterTree->Branch("eCluster_l3",&eCluster_l3);
    clusterTree->Branch("xCluster_l3",&xCluster_l3);
    clusterTree->Branch("yCluster_l3",&yCluster_l3);

    clusterTree->Branch("eCluster_l4",&eCluster_l4);
    clusterTree->Branch("xCluster_l4",&xCluster_l4);
    clusterTree->Branch("yCluster_l4",&yCluster_l4);

    clusterTree->Branch("eCluster_l5",&eCluster_l5);
    clusterTree->Branch("xCluster_l5",&xCluster_l5);
    clusterTree->Branch("yCluster_l5",&yCluster_l5);

    clusterTree->Branch("eCluster_l6",&eCluster_l6);
    clusterTree->Branch("xCluster_l6",&xCluster_l6);
    clusterTree->Branch("yCluster_l6",&yCluster_l6);

    
    clusterTree->Branch("epreCluster",&epreCluster);
    clusterTree->Branch("xpreCluster",&xpreCluster);
    clusterTree->Branch("ypreCluster",&ypreCluster);

    clusterTree->Branch("epreCluster_l1",&epreCluster_l1);
    clusterTree->Branch("xpreCluster_l1",&xpreCluster_l1);
    clusterTree->Branch("ypreCluster_l1",&ypreCluster_l1);

    
    clusterTree->Branch("epreCluster_l2",&epreCluster_l2);
    clusterTree->Branch("xpreCluster_l2",&xpreCluster_l2);
    clusterTree->Branch("ypreCluster_l2",&ypreCluster_l2);


    clusterTree->Branch("epreCluster_l3",&epreCluster_l3);
    clusterTree->Branch("xpreCluster_l3",&xpreCluster_l3);
    clusterTree->Branch("ypreCluster_l3",&ypreCluster_l3);


    clusterTree->Branch("epreCluster_l4",&epreCluster_l4);
    clusterTree->Branch("xpreCluster_l4",&xpreCluster_l4);
    clusterTree->Branch("ypreCluster_l4",&ypreCluster_l4);


    clusterTree->Branch("epreCluster_l5",&epreCluster_l5);
    clusterTree->Branch("xpreCluster_l5",&xpreCluster_l5);
    clusterTree->Branch("ypreCluster_l5",&ypreCluster_l5);


    clusterTree->Branch("epreCluster_l6",&epreCluster_l6);
    clusterTree->Branch("xpreCluster_l6",&xpreCluster_l6);
    clusterTree->Branch("ypreCluster_l6",&ypreCluster_l6);
    
    clusterTree->Branch("maxCharge_l1",&maxCharge_l1);
    clusterTree->Branch("maxCharge_l2",&maxCharge_l2);
    clusterTree->Branch("maxCharge_l3",&maxCharge_l3);
    clusterTree->Branch("maxCharge_l4",&maxCharge_l4);
    clusterTree->Branch("maxCharge_l5",&maxCharge_l5);
    clusterTree->Branch("maxCharge_l6",&maxCharge_l6);
    */
    clusterTree->Branch("conversionLayer",&terminatingVolume);
    /*
    clusterTree->Branch("ePhotonBeam",&ePhotonBeam);
    clusterTree->Branch("xPhotonBeam",&xPhotonBeam);
    clusterTree->Branch("yPhotonBeam",&yPhotonBeam);
    clusterTree->Branch("zPhotonBeam",&zPhotonBeam);
    clusterTree->Branch("xPhotonBeamEnd",&xPhotonBeamEnd);
    clusterTree->Branch("yPhotonBeamEnd",&yPhotonBeamEnd);
    clusterTree->Branch("zPhotonBeamEnd",&zPhotonBeamEnd);
    */

    clusterTree->Branch("eBeam",&eBeam);
    clusterTree->Branch("xBeam",&xBeam);
    clusterTree->Branch("yBeam",&yBeam);
    clusterTree->Branch("zBeam",&zBeam);
    clusterTree->Branch("xBeamEnd",&xBeamEnd);
    clusterTree->Branch("yBeamEnd",&yBeamEnd);
    clusterTree->Branch("zBeamEnd",&zBeamEnd);
    clusterTree->Branch("thetaBeam",&thetaBeam);
    clusterTree->Branch("phiBeam",&phiBeam);
    /*
    clusterTree->Branch("emax",&emax);
    clusterTree->Branch("emax_x",&emax_x);
    clusterTree->Branch("emax_y",&emax_y);
    clusterTree->Branch("e2max",&e2max);
    clusterTree->Branch("e2max_x",&e2max_x);
    clusterTree->Branch("e2max_y",&e2max_y);
    clusterTree->Branch("d_e12",&d_e12);
    clusterTree->Branch("dx_e12",&dx_e12);
    clusterTree->Branch("dy_e12",&dy_e12);

    clusterTree->Branch("emax_l2",&emax_l2);
    clusterTree->Branch("e2max_l2",&e2max_l2);
    clusterTree->Branch("eocore_l2",&eocore_l2);
    clusterTree->Branch("edmax_l2",&edmax_l2);
    clusterTree->Branch("w3st_l2",&w3st_l2);
    clusterTree->Branch("eFrac_l2",&eFrac_l2);
    clusterTree->Branch("eFrac1_l2",&eFrac1_l2);

    clusterTree->Branch("emax_l6",&emax_l6);
    clusterTree->Branch("e2max_l6",&e2max_l6);
    clusterTree->Branch("eocore_l6",&eocore_l6);
    clusterTree->Branch("edmax_l6",&edmax_l6);
    clusterTree->Branch("w3st_l6",&w3st_l6);
    clusterTree->Branch("w40st_l6",&w40st_l6);
    clusterTree->Branch("eFrac_l6",&eFrac_l6);
    clusterTree->Branch("eFrac1_l6",&eFrac1_l6);

    clusterTree->Branch("eFrac_l3",&eFrac_l3);
    clusterTree->Branch("eFrac_l4",&eFrac_l4);
    clusterTree->Branch("eFrac_l5",&eFrac_l5);
    clusterTree->Branch("eFrac_l6",&eFrac_l6);
    */
    //    clusterTree->Branch("myClass",&myClassVec);


    int count_dup1=0;
    int count_dup2=0;
    int count_dup3=0;
    int NumberOfEntries = myTree->GetEntries();
    std::cout << "Looping over " << NumberOfEntries << " events" << std::endl;

    double xmax = 100;
    if (energy == 3500) xmax = 0.2;
    if (energy == 1000) xmax = 0.3;
    if (energy == 100) xmax = 0.2;
    if (energy == 10) xmax = 2;
    if (energy == 1) xmax = 20;

    if (doTest) NumberOfEntries = 10;
    //   for (int entry = 0; entry < 100; entry++) {
    for (int entry = 0; entry < NumberOfEntries; entry++) {
      myTree->GetEntry(entry);

      // modified number of events for the output      
      int nev_modified = snev_modified.Atoi()+entry;
      nev_modified -= nev;
      // replace the original event ID with the new one
      eventID = nev_modified;

      // only events with hits
      if (nHits == 0) continue;
      if (doTest) std::cout << "EventID = " << eventID << std::endl;

      // calculate the theta and phi of the photon
      int nGamma = nPhotons.Atoi();
      if (doNeutrinos)
	nGamma = 0;
      bool remove_outside_detector = false;
      for (int i = 0; i < nGamma; i++) {
	thetaBeam.push_back(TMath::ATan2( sqrt( pow(xBeamEnd->at(i)-xBeam->at(i),2) + pow(yBeamEnd->at(i)-yBeam->at(i),2)),(zBeamEnd->at(i)-zBeam->at(i))));
	phiBeam.push_back(TMath::ATan2((yBeamEnd->at(i)-yBeam->at(i)),(xBeamEnd->at(i)-xBeam->at(i))));
	
	
	// remove photons that are shot outside the detector
	if (xBeam->at(i) > max_x_veto || yBeam->at(i) > max_y_veto)
	  remove_outside_detector = true;
	
	if (doTest) std::cout << "xBeam, yBeam " << xBeam->at(i) << " ," << yBeam->at(i) << std::endl;
      }
      
      if (remove_outside_detector) 
	continue;
      
      /*
      // calculate the maximum charged deposited
      double maxCharge = *max_element(QfC->begin(), QfC->end());
      //std::cout << "Max Element = " << maxCharge << std::endl;

      int maxIndex = std::max_element(QfC->begin(),QfC->end()) - QfC->begin();
      //    std::cout << "Index of maximum = " << maxIndex << std::endl;
      // calculate the max charge in each of the layers
      
      maxCharge_l1 = 0;
      maxCharge_l2 = 0;
      maxCharge_l3 = 0;
      maxCharge_l4 = 0;
      maxCharge_l5 = 0;
      maxCharge_l6 = 0;
      if (layer == 1)
	maxCharge_l1 = *max_element(QfC->begin(), QfC->end());
      if (layer == 2)
	maxCharge_l2 = *max_element(QfC->begin(), QfC->end());
      if (layer == 3)
	maxCharge_l3 = *max_element(QfC->begin(), QfC->end());
      if (layer == 4)
	maxCharge_l4 = *max_element(QfC->begin(), QfC->end());
      if (layer == 5)
	maxCharge_l5 = *max_element(QfC->begin(), QfC->end());
      if (layer == 6)
	maxCharge_l6 = *max_element(QfC->begin(), QfC->end());
      
      
      int pixelIndexX = indexX->at(maxIndex);
      int pixelIndexY = indexY->at(maxIndex);
      
      int pixelMaxX = x->at(maxIndex);
      int pixelMaxY = y->at(maxIndex);


      //      std::cout << "pixel index max X " << pixelIndexX << "; x coordinate = " << x->at(maxIndex) << std::endl;
      //      std::cout << "pixel index max Y " << pixelIndexY << "; y coordinate = " << y->at(maxIndex) << std::endl;
      */

      // very small number (epsilon) substructed from the edges to ensure correct division
      const double epsilon = 0.0001;
      // axis origin in center of detector
      //      double m_deltaXTower = (m_xMax*2./segmentationX); // size of tower in X 
      //      double m_deltaYTower = (m_yMax*2./segmentationY); // size of tower in Y 
      //      int m_nXTower = ceil(2 * (m_xMax - epsilon) / m_deltaXTower); // number of X bins
      //      int m_nYTower = ceil(2 * (m_yMax - epsilon) / m_deltaYTower); // number of Y bins
      // axis origin in (0,0)
      const double m_deltaXTower = (m_xMax/segmentationX); // size of tower in X
      const double m_deltaYTower = (m_yMax/segmentationY); // size of tower in Y
      const int m_nXTower = ceil((m_xMax - epsilon) / m_deltaXTower); // number of X bins
      const int m_nYTower = ceil((m_yMax - epsilon) / m_deltaYTower); // number of Y bins
      
      if (debug) {
	std::cout << "m_deltaXTower " << m_deltaXTower << ", m_deltaYTower " << m_deltaYTower << std::endl;
	std::cout << "m_nXTower " << m_nXTower << ", m_nYTower " << m_nYTower << std::endl;
      }
 

      ////////// Step 1: Create towers //////////
      // define the tower 2D vector where we are going store the energy for a given towerX and towerY
      // all values initialized to 0
      // tower without charge threshold
      std::vector<std::vector<double>> towers_noTh(segmentationX,std::vector<double>(segmentationY));
      // tower with charge threshold
      std::vector<std::vector<double>> towers(segmentationX,std::vector<double>(segmentationY));
      // define also a tower for each layer (actually it is not a tower anymore)
      std::vector<std::vector<double>> towers_l1(segmentationX,std::vector<double>(segmentationY));
      std::vector<std::vector<double>> towers_l2(segmentationX,std::vector<double>(segmentationY));
      std::vector<std::vector<double>> towers_l3(segmentationX,std::vector<double>(segmentationY));
      std::vector<std::vector<double>> towers_l4(segmentationX,std::vector<double>(segmentationY));
      std::vector<std::vector<double>> towers_l5(segmentationX,std::vector<double>(segmentationY));
      std::vector<std::vector<double>> towers_l6(segmentationX,std::vector<double>(segmentationY));
      // define the vector where we will store the clusters energy and coordinates
      std::vector<psCluster> preClustersClass;

      // loop over hits
      for (int hiti = 0; hiti < QfC->size(); hiti++) {


	// find out to which tower the hit belongs to
	// FIXME : maybe is better to attach pixels to a given tower and then loop over pixels and take the (x,y) positions?
	  int towerIDx = towerIdX(m_xMax, m_deltaXTower, x->at(hiti));
	  int towerIDy = towerIdY(m_yMax, m_deltaYTower, y->at(hiti));

	    
	  /*	  
	  int testIDx = towerIdX(m_xMax, m_deltaXTower, 132.40975);
	  int testIDy = towerIdY(m_yMax, m_deltaYTower, 169.70087);
	  std::cout << "test IDx = " << testIDx << " tower x " << towerX(testIDx,m_deltaXTower,m_xMax) << std::endl;
	  std::cout << "test IDy = " << testIDy << " tower y " << towerY(testIDy,m_deltaYTower,m_yMax) << std::endl;

	  
	  //testID = towerIdX(m_xMax, m_deltaXTower, 0.31025);
	  testIDx = towerIdX(m_xMax, m_deltaXTower, 0.);
	  testIDy = towerIdY(m_yMax, m_deltaYTower, 0.);
	  //	  testID = towerIdX(m_xMax, m_deltaXTower, -132.40975);
	  //testID = towerIdX(m_xMax, m_deltaXTower, -140.40975);
	  std::cout << "test IDx = " << testIDx << " tower x " << towerX(testIDx,m_deltaXTower,m_xMax) << std::endl;
	  std::cout << "test IDy = " << testIDy << " tower y " << towerY(testIDy,m_deltaYTower,m_yMax) << std::endl;
	  return 0;
	*/
	  /*
	  // if the distance between pixels is greater than the tower size, calculate the fraction
	  // fixme : implement fractional weights? Probably not priority since making towers smaller than inter pixel distance will be computationally expensive
	  double distance_between_pixels_x = 0.097725;
	  double distance_between_pixels_y = 0.05585;
	  double xCellMin = x->at(hiti) - distance_between_pixels_x * 0.5;
	  double xCellMax = x->at(hiti) + distance_between_pixels_x * 0.5;
	  double iXMin = towerIdX(xCellMin + epsilon, m_deltaXTower, x->at(hiti));
	  double iXMax = towerIdX(xCellMax - epsilon, m_deltaXTower, x->at(hiti));

	  double yCellMin = y->at(hiti) - distance_between_pixels_y * 0.5;
          double yCellMax = y->at(hiti) + distance_between_pixels_y * 0.5;
          double iYMin = towerIdY(yCellMin + epsilon, m_deltaYTower, y->at(hiti));
          double iYMax = towerIdY(yCellMax - epsilon, m_deltaYTower, y->at(hiti));
	  if (iXMin != iXMax) {
	    std::cout << "WARNING: your distance between pixels is bigger than the tower size" << std::endl;
	    std::cout << "iXmin = " << iXMin << " iXmax " << iXMax << std::endl;
	  }
	  if (iYMin != iYMax) {
	    std::cout << "WARNING: your distance between pixels is bigger than the tower size" << std::endl;
	    std::cout << "iYmin = " << iYMin << " iYmax " << iYMax << std::endl;
          }
	  */

	  // sum the energy of all the hits within a tower
	  // todo: calculate the fractional energy in case of fractional sharing of the towers
	  //std::cout << "tower IDx " << towerIDx << " yNeighbour " << yNeighbour(towerIDy,m_nYTower) << std::endl;
	  //	  std::cout << "tower IDy " << towerIDy << std::endl;
	  

	  /*	  
	  if (x->at(hiti) > 68.624749 && x->at(hiti) < 68.624751 && y->at(hiti) > 136.738524 && y->at(hiti) < 136.738526) { 
	    std::cout << "found candidate " << towerIDx << " " << yNeighbour(towerIDy,m_nYTower) << " QfC " <<  QfC->at(hiti) << std::endl;
	    std::cout << "position x " << x->at(hiti) << "position y " << y->at(hiti) << std::endl;
	  }
	  */
	  
	  // Set a pixel threshold. In the FASER case the pixel should record charges from 1fC to 64fC
	  // The simulation already groups all the hits into pixels. So hit charge = sum of charge deposited in a given pixel.
	  if (doChargeThreshold) {
	    float upperThreshold = 64.; // fC
	    float lowerThreshold = 1.; // fC
	    if (QfC->at(hiti) < lowerThreshold || QfC->at(hiti) > upperThreshold) continue;
	  }

	  towers_noTh[towerIDx][towerIDy] += QfC->at(hiti);

	  if (layer->at(hiti) == 1) towers_l1[towerIDx][towerIDy] += QfC->at(hiti);
	  if (layer->at(hiti) == 2) towers_l2[towerIDx][towerIDy] += QfC->at(hiti);
	  if (layer->at(hiti) == 3) towers_l3[towerIDx][towerIDy] += QfC->at(hiti);
	  if (layer->at(hiti) == 4) towers_l4[towerIDx][towerIDy] += QfC->at(hiti);
	  if (layer->at(hiti) == 5) towers_l5[towerIDx][towerIDy] += QfC->at(hiti);
	  if (layer->at(hiti) == 6) towers_l6[towerIDx][towerIDy] += QfC->at(hiti);




	  // fill the hexagon map
	  //	  hc->Fill(y->at(hiti),x->at(hiti),QfC->at(hiti));
      }


      

      // Upper pixel threshold = 64 fC = 0.00144 GeV
      // setting the maximum available charge per pixel to 64 fC
      // todo: take with care: this assumes each tower corresponds to a single pixel, if you change the tower size then this approach won't work
      if (doPixelChargeThreshold) { 
      for (int i = 0; i < towers.size(); i++)
	{
	  for (int j = 0; j < towers[i].size(); j++)
	    {	      
	      // 64 fC
	      //double upperThreshold = 64 * qToGeV; // GeV
	      double upperThreshold = 64; // fC
	      // 1 fC
	      double lowerThreshold = 1; // fC
	      // upper threshold (saturation)
	      if (towers_l1[i][j] > upperThreshold) towers_l1[i][j] = upperThreshold;
	      if (towers_l2[i][j] > upperThreshold) towers_l2[i][j] = upperThreshold;
	      if (towers_l3[i][j] > upperThreshold) towers_l3[i][j] = upperThreshold;
	      if (towers_l4[i][j] > upperThreshold) towers_l4[i][j] = upperThreshold;
	      if (towers_l5[i][j] > upperThreshold) towers_l5[i][j] = upperThreshold;
	      if (towers_l6[i][j] > upperThreshold) towers_l6[i][j] = upperThreshold;
	       
	      
	      // lower threshold
	      if (towers_l1[i][j] < lowerThreshold) towers_l1[i][j] = 0;
              if (towers_l2[i][j] < lowerThreshold) towers_l2[i][j] = 0;
              if (towers_l3[i][j] < lowerThreshold) towers_l3[i][j] = 0;
              if (towers_l4[i][j] < lowerThreshold) towers_l4[i][j] = 0;
              if (towers_l5[i][j] < lowerThreshold) towers_l5[i][j] = 0;
              if (towers_l6[i][j] < lowerThreshold) towers_l6[i][j] = 0;
	      
	      // sum all the single layers into a single tower
	      towers[i][j] = towers_l1[i][j] + towers_l2[i][j] + towers_l3[i][j] + towers_l4[i][j] + towers_l5[i][j] + towers_l6[i][j];
	      
	    }
	}
      }
      else {
	towers = towers_noTh;
      }
      
      /*
      // let's calculate some useful variables that we can use in the future 
      // see the definition of some variables here : arXiv:1204.1061
      emax = 0;
      e2max = 0;
      emax_l2 = 0;
      e2max_l2 = 0;
      eocore_l2 = 0;
      edmax_l2 = 0;
      w3st_l2 = 0;
      eFrac_l2 = 0;
      eFrac1_l2 = 0;
      // Variable : maximum energy deposition in a given pixel of a given layer
      // flatten the 2D vector to find the maximum
      //      std::vector<double> towers_l2_flat = flatten_2dVector(towers_l2);
      //      double pixelMaximum_l2_test = *max_element(towers_l2_flat.begin(), towers_l2_flat.end());
      //      std::cout << "pixel maximum " << pixelMaximum_l2_test << std::endl;
      
      std::vector<double> pixelMax_vec = find_max_and_position(towers);
      emax =  pixelMax_vec[0];
      double pixelMax_x = pixelMax_vec[1];
      double pixelMax_y = pixelMax_vec[2];
      emax_x = towerX(pixelMax_x,m_deltaXTower,m_xMax);
      emax_y = towerY(pixelMax_y,m_deltaYTower,m_yMax);
      std::vector<double> pixelMax_2nd_vec = find_2nd_max_and_position(towers,emax, emax_x, emax_y);
      e2max = pixelMax_2nd_vec[0];
      double pixelMax_2nd_x = pixelMax_2nd_vec[1];
      double pixelMax_2nd_y = pixelMax_2nd_vec[2];
      e2max_x = towerX(pixelMax_2nd_x,m_deltaXTower,m_xMax);
      e2max_y = towerY(pixelMax_2nd_y,m_deltaYTower,m_yMax);

      // distance between maximum and 2nd maximum
      d_e12 = 0;
      dx_e12 = 0;
      dy_e12 = 0;
      dx_e12 = sqrt((emax_x-e2max_x)*(emax_x-e2max_x));
      dy_e12 = sqrt((emax_y-e2max_y)*(emax_y-e2max_y));
      d_e12 = sqrt(dx_e12*dx_e12 + dy_e12*dy_e12);
      
      */
      /*
      std::vector<double> pixelMax_l2_vec = find_max_and_position(towers_l2);
      std::vector<double> pixelMax_l6_vec = find_max_and_position(towers_l6);
      //     double emax_l2 = pixelMax_l2_vec[0];
      emax_l2 = pixelMax_l2_vec[0];
      double pixelMax_l2_x = pixelMax_l2_vec[1];
      double pixelMax_l2_y = pixelMax_l2_vec[2];
      emax_l6 = pixelMax_l6_vec[0];
      double pixelMax_l6_x = pixelMax_l6_vec[1];
      double pixelMax_l6_y = pixelMax_l6_vec[2];
      //      std::cout << "1st pixel maximum " << emax_l2  << "[i] " << pixelMax_l2_x << " [j] " <<  pixelMax_l2_y << std::endl;
      // Variable : 2nd maximum energy deposition in a given pixel of a given layer
      std::vector<double> pixelMax_l2_2nd_vec = find_2nd_max_and_position(towers_l2,emax_l2, pixelMax_l2_x, pixelMax_l2_y);
      std::vector<double> pixelMax_l6_2nd_vec = find_2nd_max_and_position(towers_l6,emax_l6, pixelMax_l6_x, pixelMax_l6_y);
      e2max_l2 = pixelMax_l2_2nd_vec[0];
      double pixelMax_l2_2nd_x = pixelMax_l2_2nd_vec[1];
      double pixelMax_l2_2nd_y = pixelMax_l2_2nd_vec[2];
      e2max_l6 = pixelMax_l6_2nd_vec[0];
      double pixelMax_l6_2nd_x = pixelMax_l6_2nd_vec[1];
      double pixelMax_l6_2nd_y = pixelMax_l6_2nd_vec[2];
      //      std::cout << "2nd pixel maximum " << e2max_l2  << "[i] " << pixelMax_l2_2nd_x << " [j] " <<  pixelMax_l2_2nd_y << std::endl;
      
      // Variable: energy difference between the 2nd maximum and the minimal energy deposit in the valley between the maximal energy deposit and the second energy maximum
      edmax_l2 = calc_edmax(towers_l2,emax_l2,pixelMax_l2_x,pixelMax_l2_y,e2max_l2,pixelMax_l2_2nd_x,pixelMax_l2_2nd_y);
      edmax_l6 = calc_edmax(towers_l6,emax_l6,pixelMax_l6_x,pixelMax_l6_y,e2max_l6,pixelMax_l6_2nd_x,pixelMax_l6_2nd_y);
      
      // Variable: Fraction of energy deposited around the shower centre
      eocore_l2 = calc_eocore(towers_l2,emax_l2, pixelMax_l2_x, pixelMax_l2_y,3);
      eocore_l6 = calc_eocore(towers_l6,emax_l6, pixelMax_l6_x, pixelMax_l6_y,3);
      
      // Variable: shower width
      w3st_l2 = calc_wnst(towers_l2,emax_l2, pixelMax_l2_x, pixelMax_l2_y,3);
      w3st_l6 = calc_wnst(towers_l6,emax_l6, pixelMax_l6_x, pixelMax_l6_y,3);
      w40st_l6 = calc_wnst(towers_l6,emax_l6, pixelMax_l6_x, pixelMax_l6_y,40);
      
      // Variable: energy deposited in the n-th layer divided by the total energy deposited in all layers
       if (QfCtot > 0) { 
	 // eFrac_l1 = QfC_l1/QfCtot;
	 eFrac_l2 = QfC_l2/QfCtot;
	 eFrac_l3 = QfC_l3/QfCtot;
	 eFrac_l4 = QfC_l4/QfCtot;
	 eFrac_l5 = QfC_l5/QfCtot;
	 eFrac_l6 = QfC_l6/QfCtot;
       }
      // Variable: energy deposited in the n-th layer divided by the total energy deposited in the first layer
       if (QfC_l1 > 0) {
	 eFrac1_l2 = QfC_l2/QfC_l1;
	 // eFrac1_l3 = QfC_l3/QfC_l1;
	 // eFrac1_l4 = QfC_l4/QfC_l1;
	 // eFrac1_l5 = QfC_l5/QfC_l1;
	 eFrac1_l6 = QfC_l6/QfC_l1;
       }
      */


      // vector with per layer information
      std::vector<std::vector<std::vector<double>>> towers_vec;
      if (doLayersMerged) {
        towers_vec.push_back(towers); // all layers merged
      }
      else if (doThirdLayer) {
        towers_vec.push_back(towers_l3);
      }
      else {
	towers_vec.push_back(towers_l1); // L1
	towers_vec.push_back(towers_l2); // ...
	towers_vec.push_back(towers_l3);
	towers_vec.push_back(towers_l4);
	towers_vec.push_back(towers_l5);
	towers_vec.push_back(towers_l6); // L6
      }


      // Now that we have our towers, define a window (in units of tower size) which we are going to slide across the tower grid
      /// Size of the window in X for pre-clusters (in units of tower size) // ATLAS used a value of 5x5 // FCC studies used 7x15
      //      int m_nXWindow = 1;
      /// Size of the window in Y for pre-clusters (in units of tower size) // ATLAS used a value of 5x5 // FCC studies used 7x15
      //     int m_nYWindow = 1;
      
      // Now that we have our towers, define a window (in units of tower size) which we are going to slide across the tower grid
      ////////// Step 2: Find local maxima with sliding window, build preclusters, calculate their barycentre position /////////

      // fixme : Position is recalculated using the window size in X x Y (nXPosition, nYPosition) that may be smaller than the sliding window to reduce the noise influence but for the time being I set it to the same size as the windows
      // ATLAS uses 3x3, FCC 3x11
      double m_nXPosition = m_nXWindow;
      double m_nYPosition = m_nYWindow;
      int halfXPos = floor(m_nXPosition / 2.);
      int halfYPos = floor(m_nYPosition / 2.);
      double posX = 0;
      double posY = 0;
      double sumEnergyPos = 0;

      // final cluster window
      // fixme : final position can also have different size as nXPosition and nYPosition to include as much energy as possible in the cluster. 
      // for the time being  same as for m_nXPosition and m_nYPosition
      double m_nXFinal = m_nXWindow;
      double m_nYFinal = m_nYWindow;
      int halfXFin = floor(m_nXFinal / 2.);
      int halfYFin = floor(m_nYFinal / 2.);
      double sumEnergyFin = 0;
      int idXFin = 0;
      int idYFin = 0;
    

      if (m_nXTower < m_nXWindow) {
	std::cout << "Window size in X too small!!! Window " << m_nXWindow << " # of x towers " << m_nXTower << std::endl;
	return;
      }

      if (m_nYTower < m_nYWindow) {
	std::cout << "Window size in Y too small!!! Window " << m_nYWindow << " # of Y towers " << m_nYTower <<std::endl;
	return;
      }

      // loop over all the layers in case you want to build clusters for separate layers
      for (int ilayer = 0; ilayer < towers_vec.size(); ilayer++) {
	towers = towers_vec[ilayer];

	// calculate the sum of first m_nYWindow bins in X, for each Y tower
	std::vector<double> sumOverX(m_nYTower, 0);
	// summing all energies for a given window row, but only for the first X window
	// this operation will perform the sum of all energies in the first X window of the towers 2D vector, for each Y tower.
	for (int iX = 0; iX < m_nXWindow; iX++) {
	  std::transform(sumOverX.begin(), sumOverX.end(), towers[iX].begin(), sumOverX.begin(),
			 std::plus<double>());
	}
          
	
	// clear the vectors where we are going to store the positions
	preClustersClass.clear();

	//double m_energyThreshold = 1e-12;
	// energy thresholds
	if (ilayer == 0) {
	  m_energyThreshold = m_energyThreshold;
	  //m_energyThreshold = 1.;
	  //m_energyThreshold = e2max/4.;
	  //m_energyThreshold = e2max/3.5;
	  //m_energyThreshold = e2max/3.;
	  // m_energyThreshold = e2max/2;
	  //m_energyThreshold = e2max/2.3;
	  //m_energyThreshold = e2max/1.5;
	  // m_energyThreshold = e2max/1.;

	  // m_energyThreshold = emax/1.5;
	  // m_energyThreshold = emax/2.;
	  // m_energyThreshold = 1./1000.; // 1 TeV photons (window 1x1)
	  // m_energyThreshold = 44 * qToGeV; // 1 TeV photons (window 1x1)
	  // m_energyThreshold = emax/2.;
	
	  // m_energyThreshold = 1./700.; // 1 TeV photons (window 1x1)
	  // m_energyThreshold = 1./1200.; // 1 TeV photons (window 1x1)
	  // m_energyThreshold = 1./400.; // 1 TeV photons (window 2x2)
	  // m_energyThreshold = 1./500.; // random energy photons (window 2x2)
	  // m_energyThreshold = 1./700.; // random energy photons (window 5x5)

	  // m_energyThreshold = emax/3.;
	  

	  // m_energyThreshold = e2max/1.5;
	  // m_energyThreshold = e2max/3.;
	  // m_energyThreshold = e2max/2.2;
	  
	  // m_energyThreshold = 10 * qToGeV; // for 100 GeV photon
	  // m_energyThreshold = 5 * qToGeV; // for 100 GeV photon (only layer 3)
	}
	if (ilayer == 1)
	  m_energyThreshold = 1;
	if (ilayer == 2)
	  m_energyThreshold = 3;
	if (ilayer == 3)
	  m_energyThreshold = 5;
	if (ilayer == 4)
	  m_energyThreshold = 5;
	if (ilayer == 5)
	  m_energyThreshold = 5;
	if (ilayer == 6)
	  m_energyThreshold = 1000;

      
      // loop over all X slices starting at the half of the first window
      int halfXWin = floor(m_nXWindow / 2.);
      int halfYWin = floor(m_nYWindow / 2.);
      double sumWindow = 0;
      double sumYSlicePrevXWin = 0;
      double sumYSliceNextXWin = 0;
      double sumFirstYSlice = 0;
      double sumLastYSlice = 0;
      bool toRemove = false;


      // one slice in x = window, now only sum over window elements in y
      for (int iX = halfXWin; iX < m_nXTower - halfXWin; iX++) {
	//	std::cout << "iX " << iX << std::endl;
	// sum the first window in y
	for (int iYWindow = -halfYWin; iYWindow <= halfYWin; iYWindow++) {
	  //	  std::cout << "iYWindow " << iYWindow << std::endl;
	  sumWindow += sumOverX[iYWindow];
	}

	// loop over all the Y slices
	for (int iY = 0; iY < m_nYTower; iY++) {
	  //	  std::cout << "iY = " << iY << std::endl;
	  // if energy is above threshold, it may be a precluster
	  if (sumWindow > m_energyThreshold) {
	    // test local maximum in Y
	    // check closest neighbour on the right
	    if (sumOverX[iY - halfYWin] < sumOverX[iY + halfYWin + 1]) {
	      toRemove = true;
	    }
	    // check closest neighbour on the left
	    if (sumOverX[iY + halfYWin] < sumOverX[iY - halfYWin - 1]) {
	      toRemove = true;
	    }
	    // test local maximum in X
	    // check closest neighbour on the right (if it is not the first window)
	    if (iX > halfXWin) {
	      for (int iYWindowLocalCheck = iY - halfYWin; iYWindowLocalCheck <= iY + halfYWin;
		   iYWindowLocalCheck++) {
		sumYSlicePrevXWin += towers[iX - halfXWin - 1][iYWindowLocalCheck];
		sumLastYSlice += towers[iX + halfXWin][iYWindowLocalCheck];
	      }
	      if (sumYSlicePrevXWin > sumLastYSlice) {
		toRemove = true;
	      }
	    }


	    // check closest neighbour on the left (if it is not the last window)
	    if (iX < m_nXTower - halfXWin - 1) {
	      for (int iYWindowLocalCheck = iY - halfYWin; iYWindowLocalCheck <= iY + halfYWin;
		   iYWindowLocalCheck++) {
		sumYSliceNextXWin += towers[iX + halfXWin + 1][iYWindowLocalCheck];
		sumFirstYSlice += towers[iX - halfXWin][iYWindowLocalCheck];
	      }
	      if (sumYSliceNextXWin > sumFirstYSlice) {
		toRemove = true;
	      }
	    }
	    sumFirstYSlice = 0;
	    sumLastYSlice = 0;
	    sumYSlicePrevXWin = 0;
	    sumYSliceNextXWin = 0;
	    // Build precluster
	    if (!toRemove) {
	      // Calculate barycentre position (usually smaller window used to reduce noise influence)
	      posX = 0;
	      posY = 0;
	      sumEnergyPos = 0;
	      // weighted mean for position in x and y
	      for (int ipX = iX - halfXPos; ipX <= iX + halfXPos; ipX++) {
		for (int ipY = iY - halfYPos; ipY <= iY + halfYPos; ipY++) {
		  posX += towerX(ipX,m_deltaXTower,m_xMax) * towers[ipX][ipY];
		  posY += towerY(ipY,m_deltaYTower,m_yMax) * towers[ipX][ipY];
		  //std::cout << towers[ipX][ipY] << std::endl;
		  sumEnergyPos += towers[ipX][ipY];
		}
	      }

	      // std::cout << "pos X = " << posX << " posY = " << posY << std::endl;
	      // If too small energy in the position window, calculate the position in the whole sliding window
	      // Assigns correct position for cases with maximum energy deposits close to the border in x
	      // For FASER, we don't care as long as m_nXPosition = m_nXWindow
	      double m_energyThresholdFraction = 1e-12;
	      if (sumEnergyPos > m_energyThresholdFraction * m_energyThreshold) {
		posX /= sumEnergyPos;
		posY /= sumEnergyPos;
                //cluster_hist2d->Fill(posX,posY,sumEnergyPos);
	      } else {
		posX = 0;
		posY = 0;
		sumEnergyPos = 0;
		for (int ipX = iX - halfXWin; ipX <= iX + halfXWin; ipX++) {
		  for (int ipY = iY - halfYWin; ipY <= iY + halfYWin; ipY++) {
		    posX += towerX(ipX,m_deltaXTower,m_xMax) * towers[ipX][ipY];
		    posY += towerY(ipY,m_deltaYTower,m_yMax) * towers[ipX][ipY];
		    sumEnergyPos += towers[ipX][ipY];
		  }
		}
		posX /= sumEnergyPos;
		posY /= sumEnergyPos;
	      }
	      // fixme : these preclusters are not the final ones and should be deleted, we should use sumEnergyFin
	      psCluster preClusters;
	      preClusters.x = posX;
	      preClusters.y = posY;
	      preClusters.e = sumEnergyPos;
	      preClustersClass.push_back(preClusters);

	    

	      // One can choose a final cluster size with different size as the window in order to contain the maximum energy amount of the shower
	      if (doFinalTowers) {
		// Calculate final cluster energy
		sumEnergyFin = 0;
		// Final cluster position
		//idXFin = m_towerTool->idX(posX);
		idXFin = towerX(posX,m_deltaXTower,m_xMax);
		idYFin = towerY(posY,m_deltaYTower,m_yMax);
		// Recalculating the energy within the final cluster size
		for (int ipX = idXFin - halfXFin; ipX <= idXFin + halfXFin; ipX++) {
		  for (int ipY = idYFin - halfYFin; ipY <= idYFin + halfYFin; ipY++) {
		    if (ipX >= 0 && ipX < m_nXTower) {  // check if we are not outside of map in x
		      if (m_ellipseFinalCluster) {
			if (pow( (ipX - idXFin) / (m_nXFinal / 2.), 2) + pow( (ipY - idYFin) / (m_nYFinal / 2.), 2) < 1) {
			  sumEnergyFin += towers[ipX][ipY];
			}
		      } else {
			sumEnergyFin += towers[ipX][ipY];
			//		      std::cout << "sumEnergyFin = " << sumEnergyFin << std::endl;
		      }
		    }
		  }
		}
	      }
	      //	      std::cout << "sumEnergyFin " << sumEnergyFin << std::endl;
	      //	      std::cout << "sumEnergyPos " << sumEnergyPos << std::endl;
	      // check if changing the barycentre did not decrease energy below threshold
	      if (sumEnergyFin > m_energyThreshold) {
		//cluster newPreCluster;
		//newPreCluster.x = posX;
		//newPreCluster.y = posY;
		//newPreCluster.transEnergy = sumEnergyFin;
		//m_preClusters.push_back(newPreCluster);
		// fixme : these preclusters should be the final ones
		//preClustersX.push_back(std::make_pair(sumEnergyFin,posX));
		//preClustersY.push_back(std::make_pair(sumEnergyFin,posY));

		//		preClusters.push_back()
		//		std::cout << "cluster X " << posX << std::endl;
		//		std::cout << "cluster Y " << posY << std::endl;
		//		std::cout << "cluster energy " << sumEnergyFin << std::endl;


	      }
	    }
	  }
	  toRemove = false;
	  // finish processing that window in y, shift window to the next y tower
	  // substract first y tower in current window
	  sumWindow -= sumOverX[iY - halfYWin];
	  // add next y tower to the window
	  sumWindow += sumOverX[iY + halfYWin + 1];
	}
	sumWindow = 0;
	// finish processing that slice, shift window to next x tower
	if (iX < m_nXTower - halfXWin - 1) {
	  // substract first x slice in current window
	  std::transform(sumOverX.begin(), sumOverX.end(), towers[iX - halfXWin].begin(), sumOverX.begin(),
			 std::minus<double>());
	  // add next x slice to the window
	  std::transform(sumOverX.begin(), sumOverX.end(), towers[iX + halfXWin + 1].begin(), sumOverX.begin(),
			 std::plus<double>());
	}
      }
      

      if (debug)
	std::cout << "Pre-clusters size before duplicates removal: " << preClustersClass.size() << std::endl;


      // 4. Sort the preclusters according to the transverse energy (descending)
      std::sort(preClustersClass.begin(), preClustersClass.end(),[](const psCluster clu1, const psCluster clu2){return clu1.e > clu2.e;} );

      // fill histogram before duplicates removal for checks
      for (int it1 = 0; it1 != preClustersClass.size(); it1++) {
	if (debug) {
	  std::cout << "Energy of the cluster = " << preClustersClass[it1].e << " GeV" << "; x = " << preClustersClass[it1].x << "; y = " << preClustersClass[it1].y << std::endl;
	}
	
	if (ilayer == 1) {
	  
	  epreCluster_l1.push_back(preClustersClass[it1].e);
	  xpreCluster_l1.push_back(preClustersClass[it1].x);
	  ypreCluster_l1.push_back(preClustersClass[it1].y);
	}

	if (ilayer == 2) {
          epreCluster_l2.push_back(preClustersClass[it1].e);
          xpreCluster_l2.push_back(preClustersClass[it1].x);
          ypreCluster_l2.push_back(preClustersClass[it1].y);
	}

	if (ilayer == 3) {
          epreCluster_l3.push_back(preClustersClass[it1].e);
          xpreCluster_l3.push_back(preClustersClass[it1].x);
          ypreCluster_l3.push_back(preClustersClass[it1].y);
        }

	if (ilayer == 4) {
          epreCluster_l4.push_back(preClustersClass[it1].e);
          xpreCluster_l4.push_back(preClustersClass[it1].x);
          ypreCluster_l4.push_back(preClustersClass[it1].y);
        }

	if (ilayer == 5) {
          epreCluster_l5.push_back(preClustersClass[it1].e);
          xpreCluster_l5.push_back(preClustersClass[it1].x);
          ypreCluster_l5.push_back(preClustersClass[it1].y);
        }

	if (ilayer == 6) {
          epreCluster_l6.push_back(preClustersClass[it1].e);
          xpreCluster_l6.push_back(preClustersClass[it1].x);
          ypreCluster_l6.push_back(preClustersClass[it1].y);
        }

	if (ilayer == 0) {
	  
	  epreCluster.push_back(preClustersClass[it1].e);
	  xpreCluster.push_back(preClustersClass[it1].x);
	  ypreCluster.push_back(preClustersClass[it1].y);
	}
      }
      if (ilayer == 0) {
        npreCluster = preClustersClass.size();
	if (doTest) std::cout << "npreCluster " << npreCluster << std::endl;
      }
      // 5. Remove duplicates
      // If two pre-clusters are found next to each other (within window nXDuplicates, nYDuplicates), the pre-cluster with lower energy is removed.
      //      for (auto it1 = preClustersClass.begin(); it1 != preClustersClass.end(); it1++) {
      for (int it1 = 0; it1 != preClustersClass.size(); it1++) {
	// loop over all clusters with energy lower than it1 (sorting), erase if too close
	//	for (auto it2 = it1 + 1; it2 != preClustersClass.end();) {
	for (int it2 = it1 + 1; it2 != preClustersClass.size();) {
	  
	  /// Size of the window in X for the overlap removal (in units of tower size) // FCC = 2, ATLAS = 2
	  const int m_nXDuplicates = 2;
	  /// Size of the window in Y for the overlap removal (in units of tower size) // FCC = 2, ATLAS = 2
	  const int m_nYDuplicates = 2;	  
	  
          int x1 = towerIdX(m_xMax,m_deltaXTower,preClustersClass[it1].x);
          int x2 = towerIdX(m_xMax,m_deltaXTower,preClustersClass[it2].x);
          int y1 = towerIdY(m_yMax,m_deltaYTower,preClustersClass[it1].y);
          int y2 = towerIdY(m_yMax,m_deltaYTower,preClustersClass[it2].y);
          int dx = abs(x1-x2);
          int dy = abs(y1-y2);
	  //	  std::cout << "preCluster x = " << x1  << " - " << x2 << " = " << dx <<std::endl;
	  //	  std::cout << "preCluster y = " << y1  << " - " << y2 << " = " << dy <<std::endl;

	  if ((abs(x1 - x2) < m_nXDuplicates)) count_dup1++;
	  if ((abs(y1 - y2) < m_nYDuplicates)) count_dup2++;
	  if (abs(y1 - y2) > m_nYTower - m_nYDuplicates ) count_dup3++;

	  
	  //if (sqrt(pow(preClustersClass[it1].x-preClustersClass[it2].x,2) + pow(preClustersClass[it1].y-preClustersClass[it2].y,2)) < 0.15) { 
	  //if ((abs(x1 - x2) < m_nXDuplicates) && (abs(y1 - y2) < m_nYDuplicates)) {
	    

	  // USED UNTIL 9th of MAY 2023
	  // case 1) dx < 2 && dy < 2 (shooting diagonally with d = 200um)
	  if ( ((dx < m_nXDuplicates) &&
		(dy < m_nYDuplicates)) ||
	       // case 2) dx = 0 && dy < 4 (shooting horizontally with deltaX = 200um)
	       ((dx == 0) &&
		(dy < 4)) ||
	       // case 3) dx <2 && dy = 0 (shooting vertically with deltaY = 200um)
	       ((dx < m_nXDuplicates) &&
		(dy == 0)) ) {
	    //		std::cout << "removing cluster with \n" << " dX = " << abs(preClustersClass[it1].x-preClustersClass[it2].x) << ", dY = " << abs(preClustersClass[it1].y-preClustersClass[it2].y) << std::endl;
	    
	    preClustersClass.erase(preClustersClass.begin() + it2);
	    
	    
	    /*
	      
	      if ((abs(x1 - x2) < m_nXDuplicates) &&
	      ((abs(y1 - y2) < m_nYDuplicates) ||
	      (abs(y1 - y2) > m_nYTower - m_nYDuplicates ))) {
	      preClustersClass.erase(preClustersClass.begin() + it2);
	      preClustersY.erase(preClustersY.begin() + it2);
	    */
	    
	  } else {
	    float cluster_distance = sqrt(pow(preClustersClass[it1].x-preClustersClass[it2].x,2) + pow(preClustersClass[it1].y-preClustersClass[it2].y,2));
	    
	    if (debug) {
	      std::cout << "keeping cluster with \n" << " dX = " << abs(preClustersClass[it1].x-preClustersClass[it2].x) << ", dY = " << abs(preClustersClass[it1].y-preClustersClass[it2].y) << std::endl;
	      std::cout << " distance " << cluster_distance << std::endl;
	      std::cout << "dx = " << dx << " dy = " << dy <<std::endl;
	      std::cout << "pos x1 = " << preClustersClass[it1].x <<" id x1 " << x1 << "; pos x2 = " << preClustersClass[it2].x << " id x2 "<< x2 <<std::endl;
	      std::cout << "pos y1 = " << preClustersClass[it1].y <<" id y1 " << y1 << "; pos y2 = " << preClustersClass[it2].y << " id y2 "<< y2 <<std::endl;
	    
	    }
	    it2++;
	  }
	}
      }
      
      if (debug)
	std::cout << "Pre-clusters size after duplicates removal: " << preClustersClass.size() << std::endl;
      // fill our histogram
      for (int it1 = 0; it1 != preClustersClass.size(); it1++) {
	//	finalCluster_hist2d->Fill(preClustersClass[it1].x,preClustersClass[it1].y,preClustersClass[it1].e);
	if (debug)
	  std::cout << "Energy of the cluster = " << preClustersClass[it1].e << " GeV" << "; x = " << preClustersClass[it1].x << "; y = " << preClustersClass[it1].y << std::endl;
	if (ilayer == 0) {
	eCluster.push_back(preClustersClass[it1].e);
	xCluster.push_back(preClustersClass[it1].x);
	yCluster.push_back(preClustersClass[it1].y);
	//	std::cout << "eCluster " << preClustersClass[it1].first << std::endl;
	if (doTest) std::cout << "xCluster, yCluster " << preClustersClass[it1].x << ", " << preClustersClass[it1].y << "\n"<< std::endl;
	}

	if (ilayer == 1) {
          eCluster_l1.push_back(preClustersClass[it1].e);
          xCluster_l1.push_back(preClustersClass[it1].x);
          yCluster_l1.push_back(preClustersClass[it1].y);
        }
	
        if (ilayer == 2) {
          eCluster_l2.push_back(preClustersClass[it1].e);
          xCluster_l2.push_back(preClustersClass[it1].x);
          yCluster_l2.push_back(preClustersClass[it1].y);
        }

	if (ilayer == 3) {
          eCluster_l3.push_back(preClustersClass[it1].e);
          xCluster_l3.push_back(preClustersClass[it1].x);
          yCluster_l3.push_back(preClustersClass[it1].y);
        }

        if (ilayer == 4) {
          eCluster_l4.push_back(preClustersClass[it1].e);
          xCluster_l4.push_back(preClustersClass[it1].x);
          yCluster_l4.push_back(preClustersClass[it1].y);
        }

        if (ilayer == 5) {
          eCluster_l5.push_back(preClustersClass[it1].e);
          xCluster_l5.push_back(preClustersClass[it1].x);
          yCluster_l5.push_back(preClustersClass[it1].y);
        }

        if (ilayer == 6) {
          eCluster_l6.push_back(preClustersClass[it1].e);
          xCluster_l6.push_back(preClustersClass[it1].x);
          yCluster_l6.push_back(preClustersClass[it1].y);
        }


	
      }
      if (ilayer == 0) {
	nCluster = preClustersClass.size();
	if (doTest) std::cout << "nClusters = " << nCluster << std::endl;
      }
      }

      clusterTree->Fill();

      // clear vectors of the output ntuple for next event
      epreCluster.clear();
      xpreCluster.clear();
      ypreCluster.clear();

      epreCluster_l1.clear();
      xpreCluster_l1.clear();
      ypreCluster_l1.clear();

      epreCluster_l2.clear();
      xpreCluster_l2.clear();
      ypreCluster_l2.clear();


      epreCluster_l3.clear();
      xpreCluster_l3.clear();
      ypreCluster_l3.clear();


      epreCluster_l4.clear();
      xpreCluster_l4.clear();
      ypreCluster_l4.clear();


      epreCluster_l5.clear();
      xpreCluster_l5.clear();
      ypreCluster_l5.clear();


      epreCluster_l6.clear();
      xpreCluster_l6.clear();
      ypreCluster_l6.clear();


      eCluster.clear();
      xCluster.clear();
      yCluster.clear();

      eCluster_l1.clear();
      xCluster_l1.clear();
      yCluster_l1.clear();

      eCluster_l2.clear();
      xCluster_l2.clear();
      yCluster_l2.clear();

      eCluster_l3.clear();
      xCluster_l3.clear();
      yCluster_l3.clear();

      eCluster_l4.clear();
      xCluster_l4.clear();
      yCluster_l4.clear();

      eCluster_l5.clear();
      xCluster_l5.clear();
      yCluster_l5.clear();

      eCluster_l6.clear();
      xCluster_l6.clear();
      yCluster_l6.clear();

      thetaBeam.clear();
      phiBeam.clear();


    }




    // lets check that the clusters found are found in the different layers
    
  // output ntuple
  outputFile->cd();
  clusterTree->Write();
  outputFile->Close();

  std::cout << "duplicates removed with first cut " << count_dup1 << std::endl;
  std::cout << "duplicates removed with second cut " << count_dup2 << std::endl;
  std::cout << "duplicates removed with third cut " << count_dup3 << std::endl;
  }


  t = clock() - t;
  printf ("Computing time: %f seconds.\n",((float)t)/CLOCKS_PER_SEC);


}

int main(int argc,char* argv[]) {
  int argCounter;
  printf("\nNumber Of Arguments Passed: %d \n",argc);
  if (argc > 10) {
    std::cout << "You passed too many arguments!" << std::endl;
    return 1;
  }
  for(argCounter=0; argCounter<argc; argCounter++) {
    std::cout << "arg " << argCounter << " : " << argv[argCounter] << std::endl;
  }

  reconstruction(argv[1], argv[2], argv[3], argv[4], argv[5], argv[6], atoi(argv[7]), atoi(argv[8]), argv[9]);
  return 0;
}
