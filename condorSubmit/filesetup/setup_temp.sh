#!/bin/bash
cd /afs/cern.ch/work/j/jsabater/unige/FASER/testProject/myProject/slidingWindow/
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
sleep 1 # sleeping is healthy
source setup.sh

cd build/
# argument 1 = jobNumber, argument 2 = photon energy, argument 3 = geometry, argument 4 = number of photons, argument 5 = separation
# argument 6 = energy threshold, argument 7 = window size X, argument 8 = window size Y, argument 9 = straight photons yes/no

jobNumber=${1}
energy=${2}
geometry=${3}
nPhotons=${4}
separation=${5}
threshold=${6}
windowx=${7}
windowy=${8}
straight=${9}



if [ ${straight} == "True" ]
then
    # straight photons
    echo "./reco ${jobNumber} ${energy} ${geometry} ${nPhotons} ${separation} ${threshold} ${windowx} ${windowy} ${straight}"
    ./reco ${jobNumber} ${energy} ${geometry} ${nPhotons} ${separation} ${threshold} ${windowx} ${windowy} ${straight}
else
    # photons with angle
    echo "./reco ${jobNumber} ${energy} ${geometry} ${nPhotons} ${separation} ${threshold} ${windowx} ${windowy}"
    ./reco ${jobNumber} ${energy} ${geometry} ${nPhotons} ${separation} ${threshold} ${windowx} ${windowy}
fi

