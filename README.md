```
source setup.sh  
  
mkdir build  
cd build  
cmake ../
cmake --build .
```
args: file_suffix, energy (GeV), geometry, nPhotons, separation (mm)  
E.g. for 1 photon, nominal geometry (3p5W), separation 0mm (because it's only one photon)
STRAIGHT photon
```
./reco 1 random 3p5W 1 0 True
```
ANGLE photon
```
./reco 1 random 3p5W 1 0
```
E.g. for 2 photons with random energies, nominal geometry (3p5W) and separation 50mm
```
./reco 1 random 3p5W 2 50
```

void reconstruction(TString fileSuffix="5", TString senergy = "random", TString geometry = "3p5W", TString nPhotons = "2", TString separation = "50") {